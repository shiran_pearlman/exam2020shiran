library(corrplot)
library(caTools)
library(rpart)
library(rpart.plot)
library(randomForest)
library(pROC)
library(ggplot2)
library(dplyr)
library(lubridate)
library(tm)
library(ROSE)

#Q1
#
setwd("C:/Users/shiran/Desktop/שנה ג סמסטר ב/כריית ידע/datafiles")
car.original<-read.csv('carInsurance.csv')
car.data<-car.original

#
str(car.data)

car<- data.frame(car.data$Age,car.data$LastContactMonth,car.data$CarInsurance,car.data$PrevAttempts,car.data$DaysPassed, car.data$CarLoan,car.data$Balance,car.data$NoOfContacts,car.data$Marital,car.data$Job)

#Q2

head(car)
str(car)
any(is.na(car))

#turn NA to managment in JOb
table(car$car.data.Job)
str(car$car.data.Job)
any(is.na(car$car.data.Job))
car$car.data.Job<-as.character(car$car.data.Job)


turn_from_NA<- function(x){
  if (is.na(x)) {
    return('managment')
  }
  return (x)
}

car$car.data.Job<-sapply(car$car.data.Job,turn_from_NA)
car$car.data.Job<-as.factor(car$car.data.Job)

as.factor(car$car.data.NoOfContacts)
as.factor(car$car.data.Marital)
str(car$car.data.Marital)
as.factor(car$car.data.PrevAttempts)
as.factor(car$car.data.CarInsurance)
str(car$car.data.CarInsurance)
car$car.data.CarInsurance<-as.factor(car$car.data.CarInsurance)
str(car$car.data.CarInsurance)

table(car$car.data.Marital)
table(car$car.data.LastContactMonth)



filter <- sample.split(car$car.data.CarInsurance, SplitRatio = 0.7)

car.train <- subset(car, filter == TRUE)
car.test <- subset(car, filter == FALSE)

any(is.na(car.train))

dim(car)
dim(car.train)
dim(car.test)

#Q3
model.dt <- rpart(car.data.CarInsurance ~ ., car.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
prediction <- predict(model.dt,car.test)


model.rf <- randomForest(car.data.CarInsurance ~ ., data = car.train,importance = TRUE)
rpart.plot(model.rf, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction.rf <- predict(model.rf,car.test)
actual <- car.test$car.data.CarInsurance

cf.rf <- table(actual,prediction.rf)
precision <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])


#
rocCurveDC <- roc(actual,prediction, direction = ">", levels = c("1", "0"))
rocCurveRF <- roc(actual,prediction.rf, direction = ">", levels = c(1, 0))



111111111111111